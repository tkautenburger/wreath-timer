FROM node
WORKDIR /app
ADD advent-wreath.js /app/advent-wreath.js
RUN npm install mqtt --save
ENTRYPOINT ["node", "/app/advent-wreath.js"]
