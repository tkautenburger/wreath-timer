Cloud Services für Adventskranz
====================================

Dieses Git-Repository enthält alle notwendigen Cloud Services zur Steuerung des "Advenerdkranzes". Zum einen den Quellcode des
Timer-Service (node.js Javascript) sowie die Kuberentes Deployment Manifests für den Timer-Service und den Mosquitto MQTT Broker.


Mosquitto MQTT Broker Deployment und Service
--------------------------------------------

Das folgende Manifest lädt den Mosquitto Public LoadBalancing Service. Die Public IP-Adresse, welche dem Servic zugewiesen wird
benötigen Sie als Konfigurationsparameter beim Start des GPIO-Controllers auf dem Raspberry PI.

```
kubectl apply -f mosquitto-service.yml
```

mosquitto-service.yml:

```
apiVersion: v1
kind: Service
metadata:
  name: mosquitto
  namespace: advent
  labels:
    component: mosquitto
spec:
  selector:
    app: mosquitto
  ports:
  - name: mosquitto
    port: 1883
    targetPort: 1883
  - name: mosquitto-web
    port: 9001
    targetPort: 9001
  type: LoadBalancer
```

Das folgende Manifest lädt das Mosquitto MQTT Broker Deployment 

```
kubectl apply -f mosquitto-deploy.yml
```

mosquitto-deploy.yml:

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mosquitto
  namespace: advent
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: mosquitto
    spec:
      containers:
      - name: mosquitto
        image: toke/mosquitto
        ports:
        - containerPort: 9001
          name: mosquitto-web
        - containerPort: 1883
          name: mosquitto

```

Adventskranz Timer ConfigMap und Deployment
--------------------------------------------

Zur Konfiguration des Adventskranz Timers ist ein Kubernetes ConfigMap anzulegen. Die dazu notwendige Konfigurationsdatei hat den Namen `wreath-config.json`. Diese muss
im ConfigMap in `gpio-config.json` umbenannt werden. Zur Anlage des ConfigMap wird der folgende Kubernetes Befehlt verwendet:

```
kubectl create configmap adventskranz-config -n advent --from-file=gpio-config.json=wreath-config.json
```

Das folgende Manifest lädt das Adventskranz Timer Deployment

```
kubectl apply -f adventskranz-deploy.yml
```

adventskranz-deploy.yml:

```
apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: adventskranz
  namespace: "advent"
spec:
  replicas: 1
  selector:
    matchExpressions:
      - key: app
        operator: In
        values:
        - adventskranz
  template:
    metadata:
      labels:
        app: adventskranz
    spec:
      containers:
      - name: adventskranz
        image: legendlime/adventskranz:1.0
        imagePullPolicy: "Always"
        env:
        - name: MQTT_IP
          value: mosquitto.advent.svc.cluster.local
        - name: MQTT_PORT
          value: "1883"
        volumeMounts:
          - mountPath: /config
            name: config
      volumes:
        - name: config
          configMap:
            name: adventskranz-config

```
Container Image selbst bauen
----------------------------
Wer sich lieber sein eigenes Docker-Image basteln will, dem sei der beigefügte Dockerfile im Repository ans Herz gelegt. Das Image kann wie folgt erzeugt werden:

```
docker build -t <my-image-path> -f Dockerfile .
```

Das Kommando muss im gleichen Verzeichnis ausgeführt werden, in dem sich `Dockerfile`und `advent-wreath.js`befinden.

Viel Spaß,
Thomas Kautenburger
