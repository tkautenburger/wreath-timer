// configuration file path
var fs = require('fs');
var filename = "/config/gpio-config.json";

// MQTT_IP or Hostname
var MQTT_IP = process.env.MQTT_IP;
var MQTT_PORT = process.env.MQTT_PORT;
var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://' + MQTT_IP + ':' + MQTT_PORT);

const ADVENT_1_PIN = 22;
const ADVENT_2_PIN = 23;
const ADVENT_3_PIN = 24;
const ADVENT_4_PIN = 25;

const TOPIC_REFRESH = 'gpc/refresh';
const TOPIC_CANDLE_OFF = 'gpc/candleOff';
const TOPIC_CANDLE_ON = 'gpc/candleOn';

var advent1 = new Date('2020-11-28T23:00:00');
var advent2 = new Date('2020-12-05T23:00:00');
var advent3 = new Date('2020-12-12T23:00:00');
var advent4 = new Date('2020-12-19T23:00:00');

// intervall to run main loop in ms (default 1 min)
const LOOP_INTERVAL = 60000;

// Get configuration from file or if not existent use default config
var globConfig = readConfig();
if (globConfig == undefined || globConfig == null) {
  console.log("Cannot read global configuration");
} else {
  advent1 = new Date(globConfig.advent1);
  advent2 = new Date(globConfig.advent2);
  advent3 = new Date(globConfig.advent3);
  advent4 = new Date(globConfig.advent4);
}

// Read config file from disk
function readConfig() {
  if (fs.existsSync(filename)) {
    let rawdata = fs.readFileSync(filename);
    return JSON.parse(rawdata);
  }
  return null;
}

// compare two dates
function checkDate(inDate) {
  const currentDate = Date.now();
  if (inDate <= currentDate) {
    return true;
  }
  else {
    return false;
  }
}

// message candle on events on the specified dates
function mainLoop() {
  if (checkDate(advent1)) {
    client.publish(TOPIC_CANDLE_ON, ADVENT_1_PIN.toString());
  } else {
    client.publish(TOPIC_CANDLE_OFF, ADVENT_1_PIN.toString());
  }
  if (checkDate(advent2)) {
    client.publish(TOPIC_CANDLE_ON, ADVENT_2_PIN.toString());
  } else {
    client.publish(TOPIC_CANDLE_OFF, ADVENT_2_PIN.toString());
  }
  if (checkDate(advent3)) {
    client.publish(TOPIC_CANDLE_ON, ADVENT_3_PIN.toString());
  } else {
    client.publish(TOPIC_CANDLE_OFF, ADVENT_3_PIN.toString());
  }
  if (checkDate(advent4)) {
    client.publish(TOPIC_CANDLE_ON, ADVENT_4_PIN.toString());
  } else {
    client.publish(TOPIC_CANDLE_OFF, ADVENT_4_PIN.toString());
  }
}

// Listen to refresh messages
client.on('connect', function(err) {
    client.subscribe(TOPIC_REFRESH, function(err) {
      if (!err) {
        console.log("MQTT listen on topic ", TOPIC_REFRESH);
      } else {
        console.log("Error while connecting to MQTT broker: ", err);
      }
    })
})

// start main loop for incoming refresh messages
client.on('message', function (topic, message) {
  if (topic === TOPIC_REFRESH) {
    mainLoop();
  }
})

// Clean up when stopping the container
function unexportOnClose() {
  console.log("advent wreath container is going down. Turning down MQTT");
  client.end();
  process.exit(0);
}

// Handle some signals that shutdown the service
process.on('SIGINT', unexportOnClose); // function to run when user closes with ctrl+c
process.on('SIGTERM', unexportOnClose); // function to run when process gets TERM signal

// check every N second the status of the main switch
var intervalId;
intervalId = setInterval( function() {
  mainLoop();
}, LOOP_INTERVAL);
